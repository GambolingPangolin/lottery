{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators       #-}


module Lottery.DRG where

import           Crypto.Error           (CryptoFailable (..))
import           Crypto.Hash            (Context, SHA3_512)
import qualified Crypto.Hash            as Cr
import qualified Crypto.Number.Generate as Cr
import           Crypto.Random          (ChaChaDRG, MonadPseudoRandom)
import qualified Crypto.Random          as Cr
import qualified Data.ByteArray         as BA
import           Data.ByteString        (ByteString)
import qualified Data.ByteString        as BS
import qualified Data.List              as L
import           Polysemy               (Embed (..), Member, Sem)
import qualified Polysemy               as P
import           Polysemy.State         (State)
import qualified Polysemy.State         as St

type DRG = Embed (MonadPseudoRandom ChaChaDRG)


runDRG :: [ByteString] -> Sem (DRG ': r) a -> Sem r a
runDRG bs = St.evalState drg . reduceDRG
    where
    drg = Cr.drgNewSeed seed

    entropy0
        = BS.take 40
        . BA.convert
        . Cr.hashFinalize
        . Cr.hashUpdates (Cr.hashInit :: Context SHA3_512)
        $ bs

    CryptoPassed seed = Cr.seedFromBinary entropy0


generateMax :: Member DRG r => Integer -> Sem r Integer
generateMax x = P.embed (Cr.generateMax x :: MonadPseudoRandom ChaChaDRG Integer)


thresholds :: (a -> Integer) -> [a] -> [(Integer, a)]
thresholds f = reverse . L.foldl' step []
    where
    step [] x            = [(f x, x)]
    step ts@((n, _):_) x = (n + f x, x) : ts


sample :: Member DRG r => (a -> Integer) -> [a] -> Sem r a
sample getWeight as = inner (thresholds getWeight as) <$> generateMax n
    where
    n = sum $ getWeight <$> as
    inner [] _      = error "unable to sample an empty list"
    inner [(_,x)] _ = x
    inner ((tx, x):xs) i
        | i < tx = x
        | otherwise = inner xs i


reduceDRG :: Sem (DRG ': r) a -> Sem (State ChaChaDRG ': r) a
reduceDRG  = P.reinterpret $ \case
    Embed x -> do
        (z, g) <- Cr.withDRG <$> St.get <*> pure x
        z <$ St.put g
