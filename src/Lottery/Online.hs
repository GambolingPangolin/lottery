{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE KindSignatures      #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Lottery.Online
    ( Decision (..)
    , onlineLottery
    ) where

import           Control.Monad   (foldM)
import           Data.Text       (Text)
import           Polysemy        (Members, Sem)
import           Polysemy.Input  (Input)
import qualified Polysemy.Input  as P
import           Polysemy.Output (Output)
import qualified Polysemy.Output as P

import           Lottery         (LotteryParams (..), Outcome (..), Participant)
import qualified Lottery         as Lo
import           Lottery.DRG     (DRG)
import qualified Lottery.DRG     as Lo


data Decision
    = Accept
    | Reject
    | InvalidateParticipant
    deriving (Eq, Show)


onlineLottery
    :: Members [Input Decision, Output Outcome] r
    => LotteryParams
    -> Sem r [Outcome]
onlineLottery LotteryParams{..}
    = Lo.runUserEntropy participants
    . fmap outcomes
    . foldM onlineRound (initState participants)
    . fmap Lo.prizeName
    $ Lo.elongate getCount =<< prizes
    where
    getCount = fromIntegral . Lo.prizeCount


data OnlineState
    = OnlineState
    { activeParticipants :: [Participant]
    , outcomes           :: [Outcome]
    }


initState :: [Participant] -> OnlineState
initState = (`OnlineState` [])


onlineRound
    :: Members [DRG, Input Decision, Output Outcome] r
    => OnlineState
    -> Text
    -> Sem r OnlineState
onlineRound s@OnlineState{..} prize = do
    p <- Lo.sample getWeight activeParticipants
    let outcome = Outcome prize $ Lo.participantId p
    P.output outcome
    d <- P.input
    onDecision d $ updateState s outcome d

    where
    getWeight = fromIntegral . Lo.ticketCount

    onDecision = \case
        Accept -> return
        _      -> (`onlineRound` prize)


updateState :: OnlineState -> Outcome -> Decision -> OnlineState
updateState s@OnlineState{..} o@(Outcome _ pid) = \case
    Accept ->
        s { outcomes = o : outcomes }

    Reject -> s

    InvalidateParticipant ->
        s { activeParticipants = filter ((/= pid) . Lo.participantId) activeParticipants }
