{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections       #-}
{-# LANGUAGE TypeOperators       #-}


module Lottery.Offline (offlineLottery) where

import           Data.Function (on)
import qualified Data.List     as L
import           Polysemy      (Member, Sem)
import qualified Polysemy      as P

import           Lottery       (LotteryParams (..), Outcome (..))
import qualified Lottery       as Lo
import           Lottery.DRG   (DRG, generateMax)


offlineLottery :: LotteryParams -> [Outcome]
offlineLottery LotteryParams{..} =
    fmap toOutcome
    . P.run
    . Lo.runUserEntropy participants
    $ intoBuckets getCount participants <$> theDraws
    where
    toOutcome (p, x) = Outcome (Lo.prizeName x) $ Lo.participantId p
    getCount         = fromIntegral . Lo.ticketCount
    getPrizeCount    = fromIntegral . Lo.prizeCount
    theDraws         = draws getPrizeCount ticketTotal prizes
    ticketTotal      = Lo.totalTicketCount participants


-- | The list of items should be sorted in ascending order according to their rank
intoBuckets
    :: (a -> Integer)
    -- ^ box sizes
    -> [a]
    -- ^ box labels
    -> [(Integer, b)]
    -- ^ list of items to put into the boxes, together with their positions
    -> [(a, b)]
intoBuckets bucketSize = inner 0
    where
    inner _ [] _ = []
    inner _ _ [] = []
    inner n ps@(p:ps') ds@((r, x):ds')
        | r < n'
        = (p, x) : inner n ps ds'

        | otherwise = inner n' ps' ds

        where
        n' = n + bucketSize p


draws
    :: Member DRG r
    => (a -> Integer)
    -- ^ multiplicity of item
    -> Integer
    -- ^ total number of tickets
    -> [a]
    -> Sem r [(Integer, a)]
draws f tc
    = fmap (L.sortBy (compare `on` fst))
    . traverse (\x -> (, x) <$> generateMax tc)
    . (>>= Lo.elongate f)
