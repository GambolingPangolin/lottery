{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}
{-# LANGUAGE TupleSections       #-}
{-# LANGUAGE TypeOperators       #-}

module Lottery where

import           Data.Aeson         (FromJSON (..), ToJSON (..))
import qualified Data.Aeson         as Ae
import qualified Data.Aeson.TH      as Ae
import           Data.Text          (Text)
import qualified Data.Text.Encoding as Text
import           Data.Word          (Word32)
import           Polysemy           (Sem)

import           Lottery.DRG        (DRG, runDRG)



newtype ParticipantId = ParticipantId { unParticipantId :: Text }
    deriving (Eq, Show, Ord)

instance FromJSON ParticipantId where
    parseJSON = fmap ParticipantId . parseJSON

instance ToJSON ParticipantId where
    toJSON = toJSON . unParticipantId


data Participant
    = Participant
    { participantId      :: ParticipantId
    , ticketCount        :: Word32
    , participantEntropy :: Text
    } deriving (Eq, Show)


data Prize
    = Prize
    { prizeName  :: Text
    , prizeCount :: Word32
    } deriving (Eq, Show)


data LotteryParams
    = LotteryParams
    { participants :: [Participant]
    , prizes       :: [Prize]
    } deriving (Eq, Show)


data Outcome
    = Outcome
    { outcomePrize  :: Text
    , outcomeWinner :: ParticipantId
    } deriving (Eq, Show)


outcomeText :: Outcome -> Text
outcomeText (Outcome name (ParticipantId pid)) = name <> " -> " <> pid


elongate :: (a -> Integer) -> a -> [a]
elongate f x = replicate (fromIntegral $ f x) x


totalTicketCount :: [Participant] -> Integer
totalTicketCount = fromIntegral . sum . fmap ticketCount


runUserEntropy :: [Participant] -> Sem (DRG ': r) a -> Sem r a
runUserEntropy = runDRG . fmap (Text.encodeUtf8 . participantEntropy)


Ae.deriveJSON Ae.defaultOptions ''Participant
Ae.deriveJSON Ae.defaultOptions ''Prize
Ae.deriveJSON Ae.defaultOptions ''LotteryParams
Ae.deriveJSON Ae.defaultOptions ''Outcome
