{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE GADTs             #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TypeOperators     #-}

module Main where

import           Control.Applicative        (optional)
import           Control.Monad              ((>=>))
import qualified Data.Aeson                 as Ae
import           Data.Bool                  (bool)
import qualified Data.ByteString            as BS
import qualified Data.ByteString.Lazy       as BSL
import qualified Data.ByteString.Lazy.Char8 as BSL8
import           Data.Function              (on)
import qualified Data.List                  as L
import           Data.Text                  (Text)
import qualified Data.Text.IO               as Text
import           Formatting                 ((%), (%.))
import qualified Formatting                 as F
import           Options.Applicative        (ParserInfo)
import qualified Options.Applicative        as Opt
import           Polysemy                   (Embed, Member, Sem)
import qualified Polysemy                   as P
import qualified Polysemy.Input             as P
import           Polysemy.Output            (Output (..))

import           Lottery                    (LotteryParams (..), Outcome,
                                             Prize (..))
import qualified Lottery                    as Lo
import qualified Lottery.Offline            as Lo
import           Lottery.Online             (Decision (..))
import qualified Lottery.Online             as Lo


data CliConfig
    = CliConfig
    { paramsPath :: FilePath
    , runOnline  :: Bool
    , outputFile :: Maybe FilePath
    }


cliOptions :: ParserInfo CliConfig
cliOptions
    = Opt.info (Opt.helper <*> opts)
    $ Opt.progDesc "run a deterministic lottery using user-contributed entropy"
    where
    opts = CliConfig <$> params <*> online <*> outFile
    params
        =  Opt.strOption
        $  Opt.short 'p'
        <> Opt.long "params"
        <> Opt.help "path to the parameters file"
        <> Opt.value "lottery.json"
    online
        =  Opt.switch
        $  Opt.short 'x'
        <> Opt.long "online"
        <> Opt.help "run the lottery in online mode"
    outFile
        = optional . Opt.strOption
        $ Opt.short 'f'
        <> Opt.long "outputFile"
        <> Opt.help "path to the outcomes output file"


main :: IO ()
main = do
    CliConfig{..} <- Opt.execParser cliOptions
    lp            <- getParameters paramsPath
    bool offlineMode (onlineMode outputFile) runOnline lp

    where
    offlineMode
        = BSL8.putStr
        . Ae.encode
        . Lo.offlineLottery


onlineMode :: Maybe FilePath -> LotteryParams -> IO ()
onlineMode outFile lp@LotteryParams{..}
    =   introduction
    >>  runOnlineMode lp
    >>= maybe (const $ return ()) save outFile

    where
    introduction
        = instructions
        >> newline
        >> prizeSummary prizes
        >> newline

    save fp = BSL.writeFile fp . Ae.encode


newline :: IO ()
newline = Text.putStrLn ""


instructions :: IO ()
instructions = Text.putStrLn "For each draw, choose Accept (a), Reject (r), or Invalidate user (i)"


prizeSummary :: [Prize] -> IO ()
prizeSummary = mapM_ printPrize
    where
    printPrize (Prize name c) = Text.putStrLn $ F.sformat template c name
    template = (F.left 3 ' '%. F.int) % "x " % F.stext


runOnlineMode :: LotteryParams -> IO [Outcome]
runOnlineMode
    = P.runM
    . P.runInputSem getDecision
    . runStdOut Lo.outcomeText
    . Lo.onlineLottery


getDecision :: Member (Embed IO) r => Sem r Decision
getDecision = P.embed inner
    where
    inner = parseDecision <$> Text.getLine >>= maybe inner return


runStdOut :: Member (Embed IO) r => (a -> Text) -> Sem (Output a ': r) b -> Sem r b
runStdOut printX = P.interpret $ \case
    Output x -> P.embed . Text.putStrLn $ printX x


parseDecision :: Text -> Maybe Decision
parseDecision = \case
    "a" -> Just Accept
    "r" -> Just Reject
    "i" -> Just InvalidateParticipant
    _   -> Nothing


getParameters :: FilePath -> IO LotteryParams
getParameters
    =   BS.readFile
    >=> either error (pure . canonicalize) . Ae.eitherDecode . BSL.fromStrict


canonicalize :: LotteryParams -> LotteryParams
canonicalize
    =   LotteryParams
    <$> L.sortBy (compare `on` Lo.participantId) . participants
    <*> L.sortBy (compare `on` Lo.prizeName)     . prizes
