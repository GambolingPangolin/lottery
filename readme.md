# lottery

This program runs a lottery to distribute prizes, relying on user contributed randomness.


## Usage

Create a file `lottery.json` file containing a collection of participants and a collection of prizes.

```json
{
  "participants": [
    {
      "participantId": "user@example.com",
      "ticketCount": 300,
      "participantEntropy": "ab80b99..."
    }
  ],
  "prizes": [
    {
      "prizeName": "grand prize",
      "prizeCount": 1
    },
    {
      "prizeName": "awesome prize",
      "prizeCount": 10
    }
  ]
}
```

The field `prizeCount` gives the number of prizes available of that type.


### Offline mode

Run the lottery in offline mode:

```shell
$ lottery -p lottery.json
```

The call produces an array of pairs associating prizes with participants.


### Online mode

In online mode, you see prize assignment proposals.  You can optionally include a path to a file to write the results.

```shell
$ lottery -p lottery.json -x -f results.json
```

For each proposal you can

- Accept (enter "a")
- Reject (enter "r")
- Invalidate the participant (enter "i") - this removes the participant from the active list


## Installation

You will need to build the program.  From scratch, here is one way:

1. obtain `cabal` from your package manager
2. install the Haskell compiler `ghc` using [ghcup][1]
3. `git clone https://gitlab.com/GambolingPangolin/lottery.git`
4. `cabal v2-install`

To run the program you'll need `~/.cabal/bin` on your path.

[1]: https://gitlab.haskell.org/haskell/ghcup
